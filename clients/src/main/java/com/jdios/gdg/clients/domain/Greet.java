package com.jdios.gdg.clients.domain;

public class Greet {
	private String message = "Hello GDG, I am here";
	
	

	public Greet() {
		super();
	}

	public Greet(String message) {
		super();
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}}
